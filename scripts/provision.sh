#!/bin/bash

# Exit on any error
set -eux

# Setup vagrant insecure private key
mkdir -m 700 /home/vagrant/.ssh
curl https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub >> /home/vagrant/.ssh/authorized_keys
chmod 600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh

# Update all packages
yum update -y
yum clean all

# Set UseDNS to no in /etc/ssh/sshd_config
# See https://github.com/hashicorp/vagrant/issues/9232
sed -i "s/#UseDNS.*/UseDNS no/g" /etc/ssh/sshd_config

# Set IO scheduler to noop
# See https://blog.centos.org/2018/05/updated-centos-vagrant-images-available-v1804-02/
# and https://access.redhat.com/solutions/5427
sed -i "s/rhgb quiet/rhgb quiet elevator=noop/g" /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg

# Fix fragmentation issue with the underlying disk
# (fill the virtual harddrive with zeros)
# dd will exit with "No space left on device"
set +e
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY

# Make sure packer doesn't quit too early
# (before the large file is removed)
sync

history -c
