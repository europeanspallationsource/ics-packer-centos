Packer template to build a CentOS 7 vagrant box
===============================================

This Packer_ template creates a base CentOS 7 Vagrant_ box.

Usage
-----

To build the box locally, run::

    $ packer build centos7.json

This will create the vagrant box under the build directory.

To release a box, you should tag the branch and push to GitLab.
Gitlab-ci will build and automatically upload the box (on tag ony).

The tag should follow the convention: "<centos version>.<ess version>"

For example:
 - 1804.01 for the first release of CentOS 7.5 1804
 - 1804.02 for a new release based on the same CentOS version

 This follows the official CentOS vagrant box convention.


.. _Packer: https://www.packer.io
.. _Vagrant: https://www.vagrantup.com
